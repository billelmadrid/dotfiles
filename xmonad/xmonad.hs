
-- normal imports
import XMonad
import XMonad.ManageHook
import Data.Monoid

-- action imports
import XMonad.Actions.WithAll (killAll, sinkAll)

-- hook imports
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP

-- layout imports
import XMonad.Layout.LayoutModifier
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.Spacing
import XMonad.Layout.ThreeColumns

-- util imports
import XMonad.Util.ClickableWorkspaces
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedScratchpad
import XMonad.Util.SpawnOnce

-- qualified imports
import qualified XMonad.StackSet as W

-- variables
myModMask :: KeyMask
myModMask = mod4Mask

myTerminal :: String
myTerminal = "kitty"

myMenu :: String
myMenu = "rofi -show drun"

myBrowser :: String
myBrowser = "firefox"

myFile :: String
myFile = "thunar"

myEditor :: String
myEditor = "emacsclient -c -a emacs"

myGaps :: Integer
myGaps = 4

myNormalBorderColor :: String
myNormalBorderColor = "#1d1f21"

myFocusedBorderColor :: String
myFocusedBorderColor = "#80a1bd"

-- workspaces
myWorkspaces = [" 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 "]

-- scratchpads
myScratchpads :: [NamedScratchpad]
myScratchpads =
  [ NS "terminal" spawnTerm findTerm manageTerm
  , NS "htop" spawnHtop findHtop manageHtop
  , NS "pulse" spawnPulse findPulse managePulse
  , NS "yazi" spawnYazi findYazi manageYazi
  ]
  where
    spawnTerm = myTerminal ++ " -T terminal"
    findTerm = title =? "terminal"
    manageTerm = customFloating $ W.RationalRect l t w h
      where
        h = 0.7
        w = 0.7
        t = 0.85 -h
        l = 0.85 -w
    spawnHtop = myTerminal ++ " -T htop -e htop"
    findHtop = title =? "htop"
    manageHtop = customFloating $ W.RationalRect l t w h
      where
        h = 0.7
        w = 0.7
        t = 0.85 -h
        l = 0.85 -w
    spawnPulse = myTerminal ++ " -T pulse -e pulsemixer"
    findPulse = title =? "pulse"
    managePulse = customFloating $ W.RationalRect l t w h
      where
        h = 0.7
        w = 0.7
        t = 0.85 -h
        l = 0.85 -w
    spawnYazi = myTerminal ++ " -T yazi -e yazi"
    findYazi = title =? "yazi"
    manageYazi = customFloating $ W.RationalRect l t w h
      where
        h = 0.7
        w = 0.7
        t = 0.85 -h
        l = 0.85 -w

-- keybindings
myKeys =  
  [ -- normal keybindings
  ("M-<Return>",              spawn (myTerminal))
  , ("M-c",                     spawn "mcomix")
  , ("M-d",                     spawn "discord")
  , ("M-e",                     spawn (myEditor))
  , ("M-o",                     spawn "libreoffice")
  , ("M-r",                     spawn (myMenu))
  , ("M-w",                     spawn (myBrowser))
  , ("M-S-<Return>",            spawn myFile)
  , ("M-S-m",                   spawn "thunderbird")
  , ("M-S-v",                   spawn "virt-manager")

  -- screenshot keybindings
  , ("M-s n",                   spawn "maim /home/bilel/screenshots/$(date +%Y%m%d-%H%M)_maim.png")
  , ("M-s d",                   spawn "maim -d 5 /home/bilel/screenshots/$(date +%Y%m%d-%H%M)_maim.png")
  , ("M-s s",                   spawn "maim -s /home/bilel/screenshots/$(date +%Y%m%d-%H%M)_maim.png")

  -- scratchpad keybindings
  , ("M-C-<Return>",            namedScratchpadAction myScratchpads "terminal")
  , ("M-C-h",                   namedScratchpadAction myScratchpads "htop")
  , ("M-C-f",                   namedScratchpadAction myScratchpads "yazi")
  , ("M-C-p",                   namedScratchpadAction myScratchpads "pulse")

  -- wallpaper keybindings
  , ("M-p s",                   spawn "sxiv -t /home/bilel/wallpapers/")
  , ("M-p r",                   spawn "feh --bg-fil --randomize /home/bilel/wallpapers/")

  -- xmonad keybindings
  , ("M-S-a",                   killAll)
  , ("M-S-t",                   sinkAll)
  , ("M-S-c",                   kill)
  , ("M-S-r",                   spawn "xmonad --recompile; xmonad --restart")

  -- workspace keybindings
  , ("M-&",                     windows $ W.greedyView $ myWorkspaces !! 0)
  , ("M-é",                     windows $ W.greedyView $ myWorkspaces !! 1)
  , ("M-\"",                    windows $ W.greedyView $ myWorkspaces !! 2)
  , ("M-'",                     windows $ W.greedyView $ myWorkspaces !! 3)
  , ("M-(",                     windows $ W.greedyView $ myWorkspaces !! 4)
  , ("M--",                     windows $ W.greedyView $ myWorkspaces !! 5)
  , ("M-è",                     windows $ W.greedyView $ myWorkspaces !! 6)
  , ("M-_",                     windows $ W.greedyView $ myWorkspaces !! 7)
  , ("M-ç",                     windows $ W.greedyView $ myWorkspaces !! 8)

  , ("M-S-&",                   windows $ W.shift $ myWorkspaces !! 0)
  , ("M-S-é",                   windows $ W.shift $ myWorkspaces !! 1)
  , ("M-S-\"",                  windows $ W.shift $ myWorkspaces !! 2)
  , ("M-S-'",                   windows $ W.shift $ myWorkspaces !! 3)
  , ("M-S-(",                   windows $ W.shift $ myWorkspaces !! 4)
  , ("M-S--",                   windows $ W.shift $ myWorkspaces !! 5)
  , ("M-S-è",                   windows $ W.shift $ myWorkspaces !! 6)
  , ("M-S-_",                   windows $ W.shift $ myWorkspaces !! 7)
  , ("M-S-ç",                   windows $ W.shift $ myWorkspaces !! 8)
  ]

-- spacing
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- layouts
tall = renamed [Replace "tall"] $ mySpacing myGaps $ Tall  1 (3/100) (1/2)
threecol = renamed [Replace "threecol"] $mySpacing myGaps $ ThreeColMid  1 (3/100) (1/2)

myLayouts = tall ||| Mirror tall ||| Full ||| threecol

-- manage Hook
myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
  [ className =? "confirm"         --> doFloat
  , className =? "file_progress"   --> doFloat
  , className =? "dialog"          --> doFloat
  , className =? "download"        --> doFloat
  , className =? "error"           --> doFloat
  , className =? "Gimp"            --> doFloat
  , className =? "notification"    --> doFloat
  , className =? "pinentry-gtk-2"  --> doFloat
  , className =? "splash"          --> doFloat
  , className =? "toolbar"         --> doFloat
  , className =? "Yad"             --> doCenterFloat
  , title =? "Mozilla Firefox"     --> doShift ( myWorkspaces !! 1 )
  , className =? "mpv"             --> doShift ( myWorkspaces !! 5 )
  , className =? "Gimp"            --> doShift ( myWorkspaces !! 8 )
  , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  
  , isFullscreen -->  doFullFloat
  ] <+> namedScratchpadManageHook myScratchpads

-- startup hook
myStartupHook :: X ()
myStartupHook  = do
  spawnOnce "pkill trayer &"
  spawnOnce "lxpolkit &"
  spawnOnce "feh --bg-fil --randomize /home/bilel/wallpapers/ &"
  spawnOnce "picom &"
  spawnOnce "dunst &"
  spawnOnce "nm-applet &"
  spawnOnce "blueman-applet &"
  spawnOnce "emacs --daemon &"
  spawn "sleep 2 && trayer --edge top --align right --widthtype request --padding 2 --SetDockType true --SetPartialStrut true --expand true --monitor 0 --transparent true --alpha 0 --tint 0x1d1f21 --height 22"

-- configuration
myConfig = def
  { modMask = myModMask
  , terminal = myTerminal
  , layoutHook = smartBorders $ myLayouts
  , workspaces = myWorkspaces
  , startupHook = myStartupHook
  , normalBorderColor = myNormalBorderColor
  , focusedBorderColor = myFocusedBorderColor
  , manageHook = myManageHook
  }

-- xmobar config
myXmobarPP :: PP
myXmobarPP = def
  { ppSep             = xmobarColor "#b294ba" "" " • "
  , ppCurrent         = xmobarColor "#b5bd68" "" . wrap "<box type=Bottom width=2 color=#b5bd68>" "</box>"
  , ppHidden          = xmobarColor "#80a1bd" "" 
  , ppHiddenNoWindows = xmobarColor "#c4c8c5" "" 
  , ppUrgent          = xmobarColor "#cc6666" "" . wrap "<box type=Both width=2 color=#cc6666>" "</box>"
  , ppLayout          = xmobarColor "#cc6666" "" 
  , ppOrder           = \(ws:l:_) -> [ws,l]
  }

-- main function
main :: IO ()
main = xmonad
  . ewmhFullscreen
  . ewmh
  . withEasySB (statusBarProp "xmobar" (clickablePP myXmobarPP)) defToggleStrutsKey
  $ myConfig
  `additionalKeysP` myKeys
