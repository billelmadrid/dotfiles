return {
	{
		"folke/tokyonight.nvim",
		lazy = false,
		priority = 1000,
		config = function()
			vim.cmd("colorscheme tokyonight-night")
		end,
	},
	{
		"neanias/everforest-nvim",
	},
	{
		"Mofiqul/dracula.nvim",
	},
	{
		"shaunsingh/nord.nvim",
	},
	{
		"shaunsingh/solarized.nvim",
	},
	{
		"rebelot/kanagawa.nvim",
	},
	{
		"ellisonleao/gruvbox.nvim",
	},
	{
		"olimorris/onedarkpro.nvim",
		priority = 1000,
	},
	{
		"wilmanbarrios/palenight.nvim",
	},
	{
		"EdenEast/nightfox.nvim",
	},
	{
		"catppuccin/nvim",
		name = "catppuccin",
		priority = 1000,
	},
	{
		"loctvl842/monokai-pro.nvim",
	},
	{
		"rose-pine/neovim",
		name = "rose-pine",
	},
}
