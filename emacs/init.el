
;; Straight.el
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        (or (bound-and-true-p straight-base-dir)
            user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
(setq straight-use-package-by-default t)

;; Evil mode
(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

;; Fonts
(set-face-attribute 'default nil :font "Iosevka Nerd Font" :height 130 :weight 'bold)
(set-face-attribute 'fixed-pitch nil :font "Iosevka Nerd Font" :height 130 :weight 'bold)
(set-face-attribute 'variable-pitch nil :font "Iosevka Nerd Font" :height 130 :weight 'bold)
(add-to-list 'default-frame-alist '(font . "Iosevka Nerd Font-13"))

;; Garbage collection
(use-package gcmh
  :init (gcmh-mode))
(setq gc-cons-threshold 800000000)
(setq gc-cons-percentage 0.6)

;; Startup message
(defun efs/display-startup-time ()
  (message "Emacs loaded in %s with %d garbage collections")
  (format "%.2f seconds"
	  (float-time
	   (time-subtract after-init-time before-init-time)))
  gcs-done)
(add-hook 'emacs-startup-hook #'efs/display-startup-time)

;; General settings
(setq ring-bell-function 'ignore)
(setq make-backup-files nil)
(save-place-mode 1)

;; Graphical user interface
(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(global-display-line-numbers-mode 1)
(setq display-line-numbers-type 'relative)
(global-visual-line-mode 1)
;;(setq-default line-spacing 0.15)

;; Icons
(use-package all-the-icons
  :if (display-graphic-p))
(use-package nerd-icons)

;; Consult
(use-package consult)

;; Marginalia
(use-package marginalia
  :init (marginalia-mode))

;; Orderless
(use-package orderless
  :custom
  (completion-styles '(orderless basic))
  (completion--category-overrides '((file (styles basic partial-completion)))))

;; Projectile
(use-package projectile
  :init (projectile-mode))

;; Vertico
(use-package vertico
  :init (vertico-mode))

;; Vertico directory
(use-package vertico-directory
  :after vertico
  :straight nil
  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

;; Which key
(use-package which-key
  :init (which-key-mode))

;; Org auto tangle
(use-package org-auto-tangle
  :hook (org-mode . org-auto-tangle-mode))

;; Org bullets
(use-package org-bullets
  :hook (org-mode . org-bullets-mode))

;; Org electric
(add-hook 'org-mode-hook (lambda ()
			   
	 (setq-local electric-pair-inhibit-predicate
		 `(lambda (c)
		(if (char-equal c ?<) t (,electric-pair-inhibit-predicate c))))))

;; Org evil
(with-eval-after-load 'evil-maps
  (define-key evil-motion-state-map (kbd "SPC") nil)
  (define-key evil-motion-state-map (kbd "TAB") nil)
  (define-key evil-motion-state-map (kbd "RET") nil))
(setq org-return-follows-link t)

;; Org indent 
(defun disable-org-indent ()
  (interactive)
  (electric-indent-mode -1))
(add-hook 'org-mode-hook 'disable-org-indent)

;; Org tempo
(use-package org-tempo
  :straight nil)

;; Toc org 
(use-package toc-org
  :hook (org-mode . toc-org-mode))

;; Autopairs
(electric-pair-mode 1)

;; Code completion
(use-package company
  :config
  (setq company-idle-delay 0.0)
  (setq company-minimum-prefix-length 2)
  (setq company-dabbrev-downcase t)
  (setq company-dabbrev-ignore-case t))
(add-hook 'after-init-hook 'global-company-mode)

;; Flycheck
(use-package flycheck)

;; Indentation
(setq standard-indent 2)
(setq sh-indentation 2)
(setq sh-basic-offset 2)
(setq lua-indent-level 2)
(setq js-indent-level 2)
(setq typescript-indent-level 2)
(setq css-indent-offset 2)
(setq emmet-indentation 2)

;; Language support
(use-package emmet-mode)
(add-hook 'sgml-mode-hook 'emmet-mode)
(add-hook 'css-mode-hook 'emmet-mode)
(add-hook 'js-mode-hook 'emmet-mode)
(add-hook 'jsx-mode-hook 'emmet-mode)
(add-hook 'typescript-mode-hook 'emmet-mode)
(add-hook 'tsx-mode-hook 'emmet-mode)
(use-package haskell-mode)
(use-package lua-mode)
(use-package php-mode)
(straight-use-package '(tsx-mode :type git :host github :repo "orzechowskid/tsx-mode.el"))
(use-package css-in-js-mode :straight '(css-in-js-mode :type git :host github :repo "orzechowskid/tree-sitter-css-in-js"))
(use-package coverlay)
(use-package origami)
(use-package coverlay)
(use-package svelte-mode)
(use-package typescript-mode)
(use-package yaml-mode)
(add-to-list 'auto-mode-alist '("\\.ts\\'" . typescript-mode))
(add-to-list 'auto-mode-alist '("\\.tsx\\'" . tsx-mode))

;; LSP
(setq read-process-output-max (* 1024 1024))
(setq lsp-log-io nil)
(use-package lsp-mode
  :init
  (setq lsp-keymap-prefix "C-c l")
  :hook
  ((js-mode . lsp-deferred)
  (typescript-mode . lsp-deferred)
  (tsx-mode . lsp-deferred)
  (python-mode . lsp-deferred)
  (php-mode . lsp-deferred)
  (lsp-mode . lsp-enable-which-key-integration))
    :commands (lsp lsp-deferred))
(setq lsp-idle-delay 0.300)
(use-package lsp-ui
  :config
  (setq lsp-ui-doc-enable t)
  :commands (lsp-ui-mode))
(defun baal-setup-lsp-company ()
  (setq-local company-backends
              '(company-capf company-dabbrev company-dabbrev-code)))
(add-hook 'lsp-mode-hook 'lsp-ui-mode)
(add-hook 'lsp-completion-mode-hook #'baal-setup-lsp-company)

;; Magit
(use-package magit)

;; Prettier
(setq prettier-js-args '(
  "--single-quote" "true"
  "--trailing-comma" "all"
  "--bracket-spacing" "true"
  "--tab-width" "2"
))
(use-package prettier-js)
(add-hook 'js-mode-hook 'prettier-js-mode)
(add-hook 'jsx-mode-hook 'prettier-js-mode)
(add-hook 'typescript-mode-hook 'prettier-js-mode)
(add-hook 'tsx-mode-hook 'prettier-js-mode)
(add-hook 'css-mode-hook 'prettier-js-mode)
(add-hook 'html-mode-hook 'prettier-js-mode)

;; Rainbow mode
(use-package rainbow-mode
  :hook (prog-mode . rainbow-mode)
  (org-mode . rainbow-mode)
  (conf-mode . rainbow-mode))
(use-package rainbow-delimiters
  :hook (rainbow-mode . rainbow-delimiters-mode))

;; Treesitter
(use-package tree-sitter)
(use-package tree-sitter-langs)
(setq treesit-language-source-alist
  '((bash . ("https://github.com/tree-sitter/tree-sitter-bash"))
  (c . ("https://github.com/tree-sitter/tree-sitter-c"))
  (cpp . ("https://github.com/tree-sitter/tree-sitter-cpp"))
  (csharp . ("https://github.com/tree-sitter/csharp-tree-sitter"))
  (css . ("https://github.com/tree-sitter/tree-sitter-css"))
  (cmake . ("https://github.com/uyha/tree-sitter-cmake"))
  (go . ("https://github.com/tree-sitter/tree-sitter-go"))
  (hashell . ("https://github.com/tree-sitter/haskell-tree-sitter"))
  (html . ("https://github.com/tree-sitter/tree-sitter-html"))
  (java . ("https://github.com/tree-sitter/tree-sitter-java"))
  (javascript . ("https://github.com/tree-sitter/tree-sitter-javascript"))
  (json . ("https://github.com/tree-sitter/tree-sitter-json"))
  (lua . ("https://github.com/azganoth/tree-sitter-lua"))
  (make . ("https://github.com/alemuller/tree-sitter-make"))
  (ocaml . ("https://github.com/tree-sitter/tree-sitter-ocaml" "master" "ocaml/src"))
  (python . ("https://github.com/tree-sitter/tree-sitter-python"))
  (php . ("https://github.com/tree-sitter/tree-sitter-php"))
  (typescript . ("https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src"))
  (tsx . ("https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src"))
  (ruby . ("https://github.com/tree-sitter/tree-sitter-ruby"))
  (rust . ("https://github.com/tree-sitter/tree-sitter-rust"))
  (sql . ("https://github.com/m-novikov/tree-sitter-sql"))
  (toml . ("https://github.com/tree-sitter/tree-sitter-toml"))
  (zig . ("https://github.com/grayjack/tree-sitter-zig"))))

(defun nf/treesit-install-all-languages ()
  "install all languages specified by `treesit-language-source-alist'."
  (interactive)
  (let ((languages (mapcar 'car treesit-language-source-alist)))
    (dolist (lang languages)
(treesit-install-language-grammar lang)
(message "`%s' parser was installed." lang)
(sit-for 0.75))))

(defun tree-sitter-enable () 
  (interactive)
  (tree-sitter-mode 1)
  (tree-sitter-hl-mode 1))
(add-hook 'sh-mode-hook 'tree-sitter-enable)
(add-hook 'c-mode-hook 'tree-sitter-enable)
(add-hook 'css-mode-hook 'tree-sitter-enable)
(add-hook 'html-mode-hook 'tree-sitter-enable)
(add-hook 'csharp-mode-hook 'tree-sitter-enable)
(add-hook 'c++-mode-hook 'tree-sitter-enable)
(add-hook 'haskell-mode-hook 'tree-sitter-enable)
(add-hook 'php-mode-hook 'tree-sitter-enable)
(add-hook 'ruby-mode-hook 'tree-sitter-enable)
(add-hook 'java-mode-hook 'tree-sitter-enable)
(add-hook 'js-mode-hook 'tree-sitter-enable)
(add-hook 'typescript-mode-hook 'tree-sitter-enable)
(add-hook 'js-jsx-mode-hook 'tree-sitter-enable)
(add-hook 'tsx-mode-hook 'tree-sitter-enable)

;; Themes
(use-package doom-themes)
(setq doom-enable-bold t)
(setq doom-enable-italic t)
(load-theme 'doom-gruvbox t)

;; Modeline
(use-package doom-modeline
  :config
  (setq doom-modeline-height 25)
  :init (doom-modeline-mode))

;; Dashboard
(use-package dashboard
  :straight t
  :init 
  (setq dashboard-display-icons-p t)
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-banner-logo-title "Emacs is better than your editor")
  (setq dashboard-startup-banner 3)
  (setq dashboard-center-content t)
  (setq dashboard-show-shortcuts t)
  (setq dashboard-items '((recents  . 5)
		      (projects . 5)
		      (agenda . 5)))
  :config
  (dashboard-setup-startup-hook))
(setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))

;; Keybindings
(use-package general
  :init 
  (general-evil-setup)
  :config 
  (general-create-definer leader
    :states '(normal visual emacs insert)
    :keymaps 'override
    :prefix "SPC"
    :global-prefix "M-SPC"))

(leader
  "b" '(:ignore t :wk "Buffer")
  "b b" '(consult-buffer :wk "Switch Buffer")
  "b i" '(ibuffer :wk "IBuffer")
  "b k" '(kill-buffer :wk "Kill Buffer")
  "b K" '(kill-this-buffer :wk "Kill current Buffer")
  "b n" '(next-buffer :wk "Next Buffer")
  "b p" '(previous-buffer :wk "Previous Buffer"))

(leader
  "e" '(:ignore t :wk "Evaluation")
  "e b" '(eval-buffer :wk "Evaluate buffer")
  "e d" '(eval-defun :wk "Evaluate defun")
  "e e" '(eval-expression :wk "Evaluate expression")
  "e l" '(eval-last-sexp :wk "Evaluate last sexp")
  "e r" '(eval-region :wk "Evaluate region")) 

(leader
  "f" '(:ignore t :wk "File")
  "f c" '((lambda () (interactive) (load-file "~/.config/emacs/README.org")) :wk "Find config")
  "f f" '(find-file :wk "Find file")
  "f r" '(consult-recent-file :wk "Recent files")
)

(leader
  "i" '(:ignore t :wk "Indentation")
  "i r" '(indent-region :wk "Indent region")
)

(leader
  "." '(find-file :wk "Find file")
  "TAB TAB" '(comment-line :wk "Comment line")
  "h t" '(consult-theme :wk "Load themes")
  "g m" '(magit :wk "Magit")
)

(leader
  "t" '(:ignore t :wk "Terminals")
  "t a" '(async-shell-command :wk "Shell command terminal")
  "t t" '(ansi-term :wk "Ansi terminal")
  "t e" '(eshell :wk "Eshell"))
