# git integration
autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst

# prompt
PROMPT='[%b%F{green}%m@%F{cyan}%~%F{yellow}${vcs_info_msg_0_}%f] '

# completion
zstyle :compinstall filename '/home/bilel/.zshrc'
autoload -Uz compinit
compinit

# history
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000

# general settings
setopt autocd
unsetopt beep
bindkey -e

# aliases
alias ls="eza -F --icons"
alias la="eza -aF --icons"
alias ll="eza -alF --group-directories-first --icons"
alias l.="eza -alF --group-directories-first --icons | rg \."
alias ..="cd .."
alias ...="cd ../.."
alias pdw="pwd"

# export 
export PATH="$HOME/.local/bin:$PATH"

# plugins
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
