-- Indentation
vim.opt.smartindent = true
vim.opt.breakindent = true
vim.opt.expandtab = true
vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2

-- Highlighting and search
vim.opt.hlsearch = true
vim.opt.smartcase = true
vim.opt.ignorecase = true

-- Cursorline
vim.opt.cursorline = true

-- Numberline
vim.opt.number = true
vim.opt.relativenumber = true
