-- Pull in the wezterm API
local wezterm = require("wezterm")

-- This will hold the configuration.
local config = wezterm.config_builder()

-- custom kanagawa lotus theme
config.colors = {
	foreground = "#545464",
	background = "#f2ecbc",
	cursor_bg = "#545464",
	cursor_fg = "#f2ecbc",
	cursor_border = "#c8c093",
	selection_fg = "#f2ecbc",
	selection_bg = "#4d699b",
	scrollbar_thumb = "#16161d",
	split = "#16161d",
	brights = { "#616161", "#d7474b", "#6f894e", "#77713f", "#5d57a3", "#766b90", "#4e8ca2", "#545464" },
	ansi = { "#f2ecbc", "#c84053", "#6e915f", "#f9d791", "#4d699b", "#624c83", "#6693bf", "#43436c" },
}

-- config.color_scheme = "Dracula"

config.font = wezterm.font("Hack Nerd Font", { weight = "Bold", italic = false })
config.enable_tab_bar = false
config.font_size = 11
config.line_height = 1.3
config.enable_wayland = false

return config
