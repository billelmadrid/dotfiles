#!/usr/bin/env bash

total_memory=$(free -m | awk '/^Mem:/{print $2}')
used_memory=$(free -m | awk '/^Mem:/{print $3}')
echo "ram: $used_memory/$total_memory GB"
