return {
	"WhoIsSethDaniel/mason-tool-installer.nvim",
	config = function()
		require("mason-tool-installer").setup({
			ensure_installed = {
				"prettierd",
				"eslint_d",
				"stylua",
				"rubocop",
				"rustywind",
				"htmlhint",
				"golangci-lint",
				"pylint",
			},
		})
	end,
}
