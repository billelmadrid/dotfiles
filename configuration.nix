# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos"; # Define your hostname.
  networking.networkmanager.enable = true;  

  # Set your time zone.
  time.timeZone = "Africa/Algiers";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "fr";
  };

  # Hyprland 
  programs.hyprland.enable = true;

  # Enable sound.
  sound.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  hardware.bluetooth = {
    enable = true;
    package = pkgs.bluez;
  };
  
  # OpenGL
  hardware.opengl.enable = true;

  # Polkit
  security.polkit.enable = true;

  # Emacs
  services.emacs = {
    enable = true;
    package = pkgs.emacs29-pgtk;
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.bilel = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "libvirtd" ]; 
  };

  # Xdg
  xdg.portal = {
    enable = true;
    extraPortals = with pkgs; [
      xdg-desktop-portal-gtk
      xdg-desktop-portal-hyprland
    ];
    config.common.default = "gtk";
  };

  # Virtualisation
  programs.dconf.enable = true;
  virtualisation.libvirtd.enable = true;

  # Allow unfree and insecure packages
  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.allowInsecure = true;

  # Services
  services = {
    blueman.enable = true;
    dbus.enable = true;
    flatpak.enable = true;
    gvfs.enable = true; 
    openssh.enable = true;
    printing.enable = true;
  };

  # Fonts
  fonts.packages = with pkgs; [
    noto-fonts
    noto-fonts-emoji
    noto-fonts-extra
    fira
    fira-go
    ubuntu_font_family
    amiri
    (nerdfonts.override { fonts = [ "CascadiaCode" "CodeNewRoman" "FantasqueSansMono" "Iosevka" "JetBrainsMono" "Recursive" "Lilex" "Mononoki" "Monofur" "RobotoMono" "VictorMono" ]; })
  ];

  # List packages installed in system profile. To search, run:
  environment.systemPackages = with pkgs; [
    # Tools
    curl
    wget
    libtool
    libsecret
    libnotify
    unrar
    unzip
    zip
    p7zip
    eza
    ripgrep
    bat
    tree-sitter
    qemu_full
    ffmpeg
    appimage-run
    psmisc
    
    # Text editors
    neovim
    vscode-fhs

    # Terminal
    foot

    # Programming 
    gh
    git
    python3Full
    python311Packages.pip
    python311Packages.pipx
    nodejs
    nodePackages.npm
    nodePackages.prettier
    nodePackages.typescript
    nodePackages.typescript-language-server
    yarn
    bun
    cargo
    rustc
    gcc
    go
    jdk
    php
    postgresql
    mysql
    gnumake
    cmake 
    ruby

    # TUI apps
    neofetch
    nerdfetch
    htop
    cava
    pulsemixer
    lolcat
    figlet
    cmatrix
    rtorrent
    lynx
    tty-clock
    yazi

    # GUI apps
    discord
    bitwarden
    gimp
    gpick
    inkscape
    spotify
    obs-studio
    kdenlive
    pcmanfm
    mcomix
    figma-linux
    virt-manager

    # Internet
    firefox 
    brave
    thunderbird    

    # Office
    onlyoffice-bin
    zathura
    grim 
    slurp

    # Media
    imv
    hyprpaper
    mpv     
    swww
 
    # WM tools
    fuzzel
    mako
    swayidle
    swaylock-effects
    waybar
    ydotool

    # Themes
    arc-theme 
    nwg-look
    papirus-icon-theme
    bibata-cursors

  ];

  # Copy the NixOS configuration file and link it from the resulting system
  system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  system.stateVersion = "24.05"; 

}

