return {
	"mfussenegger/nvim-lint",
	config = function()
		local lint = require("lint")
		lint.linters_by_ft = {
			javascript = { "eslint_d" },
			typescript = { "eslint_d" },
			javascriptreact = { "eslint_d" },
			typescriptreact = { "eslint_d" },
			svelte = { "eslint_d" },
			html = { "htmlhint" },
			css = { "stylelint" },
			python = { "pylint" },
			ruby = { "rubocop" },
			go = { "golangcilint" },
		}
	end,
}
