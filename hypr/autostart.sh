#!/usr/bin/env bash

/usr/libexec/polkit-gnome-authentication-agent-1 &
gentoo-pipewire-launcher restart &
foot --server &
hyprpaper &
mako &
nm-applet &
blueman-applet &
waybar &
emacs --daemon &
