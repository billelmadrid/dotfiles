return {
	"williamboman/mason-lspconfig.nvim",
	config = function()
		require("mason-lspconfig").setup({
			ensure_installed = {
				"angularls",
				"astro",
				"bashls",
				"cssls",
				"cssmodules_ls",
				"emmet_ls",
				"graphql",
				"html",
				"jsonls",
				"lua_ls",
				"intelephense",
				"prismals",
				"pyright",
				"ruby_lsp",
				"somesass_ls",
				"sqlls",
				"svelte",
				"tailwindcss",
				"tsserver",
				"volar",
			},
			automatic_installation = true,
		})
	end,
}
