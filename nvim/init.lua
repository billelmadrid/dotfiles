-- Global settings
vim.g.mapleader = " "
vim.g.maplocalleader = " "
vim.opt.termguicolors = true

-- Imports
require("bilel.lazy")
require("bilel.core")
