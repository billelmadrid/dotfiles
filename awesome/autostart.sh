#!/usr/bin/env bash

feh --bg-fil --randomize ~/.config/awesome/wallpapers/ &
picom &
nm-applet &
blueman-applet &
