# /etc/skel/.bashrc

# Interactive shell
if [[ $- != *i* ]] ; then
	return
fi

# Aliases
alias ls="eza -F --icons"
alias la="eza -aF --icons"
alias ll="eza -alF --group-directories-first --icons"
alias l.="eza -alF --group-directories-first --icons | grep \."
alias pdw="pwd"
alias ..="cd .."
alias ...="cd ../.."

# Exports 
export PATH="/home/bilel/.local/bin:$PATH"
