return {
	"stevearc/conform.nvim",
	config = function()
		require("conform").setup({
			formatters_by_ft = {
				css = { "prettierd" },
				html = { "prettierd", "rustywind" },
				javascript = { "prettierd", "rustywind" },
				typescript = { "prettierd", "rustywind" },
				javascriptreact = { "prettierd", "rustywind" },
				typescriptreact = { "prettierd", "rustywind" },
				markdown = { "prettierd" },
				svelte = { "prettierd" },
				vue = { "prettierd" },
				graphql = { "prettierd" },
				liquid = { "prettierd" },
				yaml = { "prettierd" },
				lua = { "stylua" },
				python = { "isort", "black" },
				ruby = { "rubocop" },
			},
			format_on_save = {
				timeout_ms = 500,
				lsp_format = "fallback",
			},
		})
	end,
}
