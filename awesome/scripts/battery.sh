#!/usr/bin/env bash

charge=$(cat /sys/class/power_supply/BAT0/capacity)
status=$(cat /sys/class/power_supply/BAT0/status)

if [[ "$status" == "Charging" ]]; then
  echo "battery: $charge% (on)"
elif [[ "$status" == "Discharging" ]]; then
  echo "battery: $charge% (off)"
elif [[ "$status" == "Full" ]]; then
  echo "Full"
else
  echo "program terminated" && exit 0
fi
